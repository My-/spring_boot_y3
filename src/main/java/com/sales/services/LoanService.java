package com.sales.services;

import com.sales.exceptions.BookAlreadyOnLoanException;
import com.sales.exceptions.LoanNotFoundException;
import com.sales.exceptions.MissingBookOrCustomerException;
import com.sales.models.Book;
import com.sales.models.Customer;
import com.sales.models.Loan;
import com.sales.repositories.LoanRepository;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static java.time.LocalDate.now;

@Service
public class LoanService {
    @Autowired
    LoanRepository loanRepository;
    @Autowired
    CustomerService customerService;
    @Autowired
    BookService bookService;

    public Iterable<Loan> findLoans() {
        return loanRepository.findAll();
    }


    public void addLoan(Loan loan) throws MissingBookOrCustomerException, BookAlreadyOnLoanException {
        Customer customer = customerService.findCustomer(loan.getCust().getcId());
        Book book = bookService.findBook(loan.getBook().getBid());

        // Customer or Book is not in DB
        if(Objects.isNull(customer) || Objects.isNull(book)){
            throw new MissingBookOrCustomerException(String.format("%s %s",
                    Objects.isNull(customer) ? "No such customer: "+ loan.getCust().getcId() : "",
                    Objects.isNull(book) ? "No such book: "+ loan.getBook().getBid() : "")
            );
        }

        loan.setCust(customer);
        loan.setBook(book);

        // build due date
        String dueDate = now()
                .plusDays(loan.getCust().getLoanPeriod())
                .toString();
        loan.setDueDate(dueDate);

        // Book in on loan
        // https://stackoverflow.com/a/44175738/5322506
        try{
            loanRepository.save(loan);
        }
        catch ( Exception e){ // ConstraintViolationException |
            throw new BookAlreadyOnLoanException(
                    String.format("Book: %s (%s) already on loan to Customer: %s (%s)",
                            loan.getBook().getBid(), loan.getBook().getTitle(),
                            loan.getCust().getcId(), loan.getCust().getcName()
                    )
            );
        }
    }

    public void deleteLoan(Loan loan) throws LoanNotFoundException {
        Loan l = loanRepository.findOne(loan.getLid());

        if( Objects.isNull(l)) {
            throw new LoanNotFoundException("No such Loan: "+ loan.getLid());
        }

        loanRepository.delete(loan);
    }
}