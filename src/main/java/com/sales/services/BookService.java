package com.sales.services;

import com.sales.models.Book;
import com.sales.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;

    public Iterable<Book> findBooks(){
        return bookRepository.findAll();
    }

    public void addBook(Book book){
        bookRepository.save(book);
    }

    public Book findBook(long id){
        return bookRepository.findOne(id);
    }
}
