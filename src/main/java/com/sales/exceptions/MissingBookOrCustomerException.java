package com.sales.exceptions;

public class MissingBookOrCustomerException extends Exception{
    public MissingBookOrCustomerException() {
    }

    public MissingBookOrCustomerException(String message) {
        super(message);
    }
}
