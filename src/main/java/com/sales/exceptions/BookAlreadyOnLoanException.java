package com.sales.exceptions;


public class BookAlreadyOnLoanException extends Exception {
    public BookAlreadyOnLoanException() { }

    public BookAlreadyOnLoanException(String message) {
        super(message);
    }
}
