package com.sales.controllers;

;
import com.sales.exceptions.BookAlreadyOnLoanException;
import com.sales.exceptions.LoanNotFoundException;
import com.sales.exceptions.MissingBookOrCustomerException;
import com.sales.models.Loan;
import com.sales.services.BookService;
import com.sales.services.CustomerService;
import com.sales.services.LoanService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;

import static java.time.LocalDate.now;

@Controller
public class LoanController {
    @Autowired
    LoanService loanService;
    @Autowired
    CustomerService customerService;
    @Autowired
    BookService bookService;

    @RequestMapping(value = "/showLoans", method = RequestMethod.GET)
    public String getLoans(Model model){
        ArrayList<Loan> loans = (ArrayList<Loan>)loanService.findLoans();
        model.addAttribute("loans", loans);

        return "showLoans";
    }

    @RequestMapping(value = "/newLoan", method = RequestMethod.GET)
    public String getNewLoan(Model model){
        Loan loan = new Loan();
        model.addAttribute("loan", loan);

        return "newLoan";
    }


    @RequestMapping(value = "/newLoan", method = RequestMethod.POST)
    public String postNewLoan(@Valid @ModelAttribute("loan") Loan loan,
                              BindingResult result,
                              Model model)
    {
        if( result.hasErrors() ){ return "newLoan"; }

        try{
            loanService.addLoan(loan);
        }
        catch (MissingBookOrCustomerException | BookAlreadyOnLoanException  e){
            model.addAttribute("error", e);
            return "forward:getLoanErrorPage";
        }

        return "redirect:showLoans";
    }


    @RequestMapping(value = "/deleteLoan", method = RequestMethod.GET)
    public String getDeleteLoan(Model model){
        Loan loan = new Loan();
        model.addAttribute("loan", loan);

        return "deleteLoan";
    }

    @RequestMapping(value = "/deleteLoan", method = RequestMethod.POST)
    public String postDeleteLoan(@Valid @ModelAttribute("loan") Loan loan,
                                 BindingResult result,
                                 Model model)
    {
        if( result.hasErrors() ){ return "deleteLoan"; }

        try {
            loanService.deleteLoan(loan);
        } catch (LoanNotFoundException e) {
            model.addAttribute("error", e);
            return "forward:getLoanDeleteErrorPage";
        }

        return "redirect:showLoans";
    }

//    @RequestMapping(value = "/deleteLoan", method = RequestMethod.DELETE)
//    public String deleteLoan(@Valid @ModelAttribute("loan") Loan loan, BindingResult result) {
//        if( result.hasErrors() ){ return "deleteLoan"; }
//        loanService.deleteLoan(loan);
//
//        return "redirect:showLoans";
//    }


    @RequestMapping(value = "/getLoanDeleteErrorPage", method = RequestMethod.POST)
    public String postDeleteErrorPage(@Valid @ModelAttribute("customer") Loan loan, BindingResult result) {
        return "errorDeleteLoan";
    }

    @RequestMapping(value = "/getLoanErrorPage", method = RequestMethod.POST)
    public String postAddBook(@Valid @ModelAttribute("customer") Loan loan, BindingResult result) {
        return "errorLoanPage";
    }

}
