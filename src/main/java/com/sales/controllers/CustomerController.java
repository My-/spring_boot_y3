package com.sales.controllers;

import com.sales.models.Book;
import com.sales.models.Customer;
import com.sales.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = "/showCustomers", method = RequestMethod.GET)
    public String getCustomers(Model model){
        ArrayList<Customer> customers = (ArrayList<Customer>)customerService.findCustomers();
        model.addAttribute("customers", customers);

        return "showCustomers";
    }

    @RequestMapping(value = "/addCustomer", method = RequestMethod.GET)
    public String postCustomer(Model model){
        Customer customer = new Customer();
        model.addAttribute("customer", customer);

        return "addCustomer";
    }

    @RequestMapping(value = "/addCustomer", method = RequestMethod.POST)
    public String postAddBook(@Valid @ModelAttribute("customer") Customer customer,
                              BindingResult result)
    {
        if( result.hasErrors() ){ return "addCustomer"; }
        customerService.addCustomer(customer);

        return "redirect:showCustomers";
    }

}
