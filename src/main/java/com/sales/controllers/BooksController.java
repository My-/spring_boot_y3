package com.sales.controllers;

import com.sales.models.Book;
import com.sales.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class BooksController {
    @Autowired
    BookService bookService;


    @RequestMapping(value = "/showBooks", method = RequestMethod.GET)
    public String getBooks(Model model){
        ArrayList<Book> books = (ArrayList<Book>)bookService.findBooks();
        model.addAttribute("books", books);

        return "showBooks";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public String getAddBook(Model model){
        Book book = new Book();
        model.addAttribute("book", book);

        return "addBooks";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String postAddBook(@Valid @ModelAttribute("book") Book book,
                              BindingResult result)
    {
        if( result.hasErrors() ){ return "addBooks"; }
        bookService.addBook(book);

        return "redirect:showBooks";
    }
}
