<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>G00339629 - Mindaugas Sharskus</title>
    <link rel="stylesheet" href="/css/w3.css">
</head>
<body>

<div class="w3-bar w3-border w3-light-grey">
    <a href="./../" class="w3-bar-item w3-button w3-mobile w3-dark-grey">Home</a>
    <a href="/addBook" class="w3-bar-item w3-button w3-mobile">Add Book</a>
    <a href="/showCustomers" class="w3-bar-item w3-button w3-mobile">List Customers</a>
    <a href="/showLoans" class="w3-bar-item w3-button w3-mobile">List Loans</a>
    <a href="/logout" class="w3-bar-item w3-button w3-mobile w3-right">Logout</a>
</div>

<h1>Could not delete loan</h1>

<%--@elvariable id="error" type="java.lang.Exception"--%>
<h4>${error.message}</h4>

</body>
</html>