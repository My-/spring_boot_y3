<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>G00339629 - Mindaugas Sharskus</title>
    <link rel="stylesheet" href="/css/w3.css">
</head>
<body>

<div class="w3-bar w3-border w3-light-grey">
    <a href="./../" class="w3-bar-item w3-button w3-mobile w3-dark-grey">Home</a>
    <a href="/addBook" class="w3-bar-item w3-button w3-mobile">Add Book</a>
    <a href="/showCustomers" class="w3-bar-item w3-button w3-mobile">List Customers</a>
    <a href="/showLoans" class="w3-bar-item w3-button w3-mobile">List Loans</a>
    <a href="/logout" class="w3-bar-item w3-button w3-mobile w3-right">Logout</a>
</div>

<h1>List of Loans</h1>

<table class="w3-table-all"  style="width:90%; margin: 40px auto auto;">
    <tr class="w3-dark-grey">
        <th>Loan ID</th>
        <th>Customer ID</th>
        <th>Customer Name</th>
        <th>Book Title</th>
        <th>Author</th>
        <th>Due Date</th>
    </tr>
    <%--@elvariable id="loans" type="java.util.List<com.sales.models.Loan>"--%>
    <c:forEach items="${loans}" var="loan">
    <tr>
        <td>${loan.lid}</td>
        <td>${loan.cust.cId}</td>
        <td>${loan.cust.cName}</td>
        <td>${loan.book.title}</td>
        <td>${loan.book.author}</td>
        <td>${loan.dueDate}</td>
    </tr>
    </c:forEach>
</table>



</body>
</html>