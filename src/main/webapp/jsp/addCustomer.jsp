<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>G00339629 - Mindaugas Sharskus</title>
    <link rel="stylesheet" href="/css/w3.css">
</head>
<body>

<div class="w3-bar w3-border w3-light-grey">
    <a href="./../" class="w3-bar-item w3-button w3-mobile w3-dark-grey">Home</a>
    <a href="/addBook" class="w3-bar-item w3-button w3-mobile">Add Book</a>
    <a href="/showCustomers" class="w3-bar-item w3-button w3-mobile">List Customers</a>
    <a href="/showLoans" class="w3-bar-item w3-button w3-mobile">List Loans</a>
    <a href="/logout" class="w3-bar-item w3-button w3-mobile w3-right">Logout</a>
</div>

<div class="w3-card-4 w3-display-middle w3-padding">
    <header class="w3-light-gray"><h1>Add New Customer</h1></header>
    <%--@elvariable id="customer" type="com.sales.models.Customer"--%>
    <form:form modelAttribute="customer">
        <table>
            <tr>
                <td>Customer Name:</td>
                <td><form:input path="cName" />
                    <form:errors path="cName"/>
                </td>
            </tr>
            <tr>
                <td>Loan Period (days):</td>
                <td><form:input path="loanPeriod" />
                    <form:errors path="loanPeriod"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Add"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>

</body>
</html>