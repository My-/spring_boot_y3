<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>G00339629 - Mindaugas Sharskus</title>
    <link rel="stylesheet" href="/css/w3.css">
</head>
<body>

<div class="w3-bar w3-border w3-light-grey">
    <a href="./../" class="w3-bar-item w3-button w3-mobile w3-dark-grey">Home</a>
    <a href="/addBook" class="w3-bar-item w3-button w3-mobile">Add Book</a>
    <a href="/showCustomers" class="w3-bar-item w3-button w3-mobile">List Customers</a>
    <a href="/showLoans" class="w3-bar-item w3-button w3-mobile">List Loans</a>
    <a href="/logout" class="w3-bar-item w3-button w3-mobile w3-right">Logout</a>
</div>

<h1>List of Customers</h1>

<%--@elvariable id="customers" type="java.util.List<com.sales.models.Customer>"--%>
<c:forEach items="${customers}" var="customer">
<div class="w3-card-4 w3-grey" style="width:90%; margin: 40px auto auto;">
    <header class="w3-container">
        <h2>${customer.cId} ${customer.cName}</h2>
        <p>Loan period = ${customer.loanPeriod} days</p>
    </header>

    <b class="w3-margin w3-margin-top w3-margin-bottom">${customer.cName} Loans</b>
    <table class="w3-table-all">
        <tr class="w3-blue-gray">
            <th>Loan ID</th>
            <th>Book ID</th>
            <th>Title</th>
            <th>Author</th>
        </tr>
        <c:forEach items="${customer.loans}" var="loan">
            <tr>
                <td>${loan.lid}</td>
                <td>${loan.book.bid}</td>
                <td>${loan.book.title}</td>
                <td>${loan.book.author}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</c:forEach>

</body>
</html>